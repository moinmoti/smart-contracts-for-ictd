pragma solidity ^0.5.0;

contract PromissoryNote{

    struct Job {
        uint jobId;
        uint requestId;
        address assignedJudge;
        string description;
        address employer;
        address employee;
        uint amount;
        uint deposit;
        bool accepted;
        bool cancelled;
        bool completed;
        bool conflict;
        bool verified;
        bool settled;
    }

    mapping (address => uint) public balance;
    mapping (uint => Job) Jobs;
    mapping (address => uint[]) jobRequests;
    mapping (address => uint[]) conflicts;
    address[] judges;
    uint numJob;
    uint judgeLottery;

    address moderator;

    constructor ()
    public {
        moderator = msg.sender;
        numJob = 0;
        judgeLottery = 0;
    }

    function updateModerator(address _moderator)
    public {
        require(moderator == msg.sender, "You are not the Moderator");
        moderator= _moderator;
    }

    function removeRequest (address employee, uint requestId)
    internal {
        uint last = jobRequests[employee].length - 1;
        jobRequests[employee][requestId] = jobRequests[employee][last];
        jobRequests[employee].length--;
    }

    function getDescription (uint _jobId)
    public view returns (string memory) {
        Job memory job = Jobs[_jobId];
        require(job.employee == msg.sender, "This job is not for you");
        return job.description;
    }

    function addJudge (address _judge)
    public {
        require(moderator == msg.sender, "You are not the Moderator");
        judges.push(_judge);
    }

    function assignJudge (uint jobId)
    internal {
        Jobs[jobId].assignedJudge = judges[judgeLottery];
        conflicts[judges[judgeLottery]].push(jobId);
        judgeLottery = (judgeLottery + 1) % judges.length;
    }

    function addBalance ()
    public payable {
        balance[msg.sender] += msg.value;
    }

    function getConflicts ()
    public view returns (uint[] memory) {
        return conflicts[msg.sender];
    }

    function getRequests ()
    public view returns (uint[] memory) {
        return jobRequests[msg.sender];
    }

    function postJob (address _employee, uint _amount, string memory _description)
    public {
        require(balance[msg.sender] >= _amount + _amount/10, "Your payment is not enough");
        Jobs[++numJob].jobId     = numJob;
        Jobs[numJob].employer    = msg.sender;
        Jobs[numJob].employee    = _employee;
        Jobs[numJob].amount      = _amount;
        Jobs[numJob].deposit     = _amount/10;
        Jobs[numJob].requestId   = jobRequests[_employee].length;
        Jobs[numJob].description = _description;
        balance[msg.sender]     -= _amount + Jobs[numJob].deposit;
        jobRequests[_employee].push(numJob);
    }

    function cancelJob (uint _jobId)
    public {
        Job memory job         = Jobs[_jobId];
        require(job.employer   == msg.sender, "You are not the Employer");
        require(job.accepted   == false, "Job already accepted");
        Jobs[_jobId].cancelled = true;
        removeRequest(job.employee, job.requestId);
    }

    function acceptJob (uint _jobId)
    public {
        Job memory job        = Jobs[_jobId];
        require(job.employee  == msg.sender, "This job is not for you");
        require(job.cancelled == false, "Job is no longer available");
        Jobs[_jobId].accepted = true;
        removeRequest(job.employee, job.requestId);
    }

    function completeJob (uint _jobId)
    public {
        Job memory job         = Jobs[_jobId];
        require(job.accepted   == true, "Job was not accepted");
        Jobs[_jobId].completed = true;
    }

    function verifyJob (uint _jobId, bool _conflict)
    public {
        Job memory job        = Jobs[_jobId];
        require(job.employer  == msg.sender, "You are not the Employer");
        require(job.completed == true, "Job is not completed");
        require(job.verified  == false, "Job already verified");
        Jobs[_jobId].verified = true;
        if (_conflict == true) {
            Jobs[_jobId].conflict = true;
            assignJudge(_jobId);
        } else {
            Jobs[_jobId].settled   = true;
            balance[job.employee] += job.amount;
            balance[job.employer] += job.deposit;
        }
    }

    function demandSettlement (uint _jobId)
    public {
        Job memory job        = Jobs[_jobId];
        require(job.employee  == msg.sender, "You are not the Employee");
        require(job.settled   == false, "Job already settled");
        require(job.conflict  == false, "Judge is assigned on the conflict");
        Jobs[_jobId].conflict = true;
        assignJudge(_jobId);
    }

    function resolveConflict(uint _jobId, bool verdict)
    public {
        Job memory job       = Jobs[_jobId];
        require(job.conflict == true, "Employer issued no conflict");
        require(job.settled  == false, "Job already settled");
        if (verdict == true) {
            Jobs[_jobId].settled   = true;
            balance[job.employee] += job.amount;
        } else {
            Jobs[_jobId].settled   = true;
            balance[job.employer] += job.amount;
        }
        balance[msg.sender] += job.deposit;
    }

    function withdraw (uint amount)
    public {
        require(balance[msg.sender] >= amount, "You do not have the required balance");
        balance[msg.sender] -= amount;
        msg.sender.transfer(amount);
    }
}
