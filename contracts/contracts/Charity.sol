pragma solidity ^0.5.0;

contract DecentralizedCharity {

    mapping (address => uint) public balance;

    address[] public fundees;
    mapping (bytes32 => bool) public tokens;
    uint public accumulated;

    address moderator;

    modifier onlyModerator () {
        require(moderator == msg.sender, "Only Moderator is allowed to access this method");
        _;
    }

    constructor ()
    public {
        moderator = msg.sender;
    }

    function updateModerator (address _moderator)
    public onlyModerator {
        moderator = _moderator;
    }

    function uploadTokens (bytes32[] memory _newTokens)
    public onlyModerator {
        for (uint i = 0; i < _newTokens.length; i++) {
            tokens[_newTokens[i]] = true;
        }
    }

    function register (string memory _key)
    public {
        require(
            tokens[keccak256(abi.encodePacked(_key))] == true,
            "Invalid token !!!"
        );
        fundees.push(msg.sender);
    }

    function distribute ()
    public {
        require(
            accumulated - fundees.length > 0,
            "Not enough funds for all fundees"
        );
        uint quota = accumulated/fundees.length;
        accumulated -= quota*fundees.length;
        for (uint i = 0; i < fundees.length; i++) {
            balance[fundees[i]] += quota;
        }
    }

    function donate ()
    public payable {
        accumulated += msg.value;
    }

    function withdraw (uint amount)
    public {
        require(
            balance[msg.sender] >= amount,
            "You do not have the required balance"
        );
        balance[msg.sender] -= amount;
        msg.sender.transfer(amount);
    }
}
